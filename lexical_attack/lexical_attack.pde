import java.util.*;
/**
 * Lexical Attack is a word display processor, aimed at virtually
 * shooting the spectator with a lexical field that comes from
 * a csv file produced by wordocc https://git.tetalab.org:8888/Mutah/wordocc
 *
 */

//- some variable to customize to your needs

// data file
String WORDS_CSV_FILE = "data/surveiller_et_punir.csv";

// foreground color
int FG_COLOR = 255;

// background color
int BG_COLOR = 0;

// main font
String FONT_FILE = "fonts/Bitstream Charter.ttf";

// bolder font for word with more score
String BOLD_FONT_FILE = "fonts/Bitstream Charter Bold.ttf";

//- end of visual customization. TODO : extract the variables of chaos

void setup() {  
  //size(1280, 720);
  fullScreen();
  
  frameRate(3);
  
  background(BG_COLOR);
  fill(FG_COLOR);
  smooth();
  
  f = createFont(FONT_FILE, 24, true);
  bf = createFont(BOLD_FONT_FILE, 24, true);
  
  loadData();
}

void draw() { 
  // let's get a random word with some chaos formula
  int id = round(random(words.size() / random(1, 12))) - 1;
  Word word = words.get(id);

  // draw a transparent rectangle over the text
  // so it disappear slowly
  fill(BG_COLOR, random(16, 32));
  rect(0, 0, width, height);

  float size = map(word.count, 1, maxCount, 40, 128);  
  if (id < words.size() / 8 && random(10) > 3) {
    textFont(bf);
    size = size * 1.33;
  } else {
    textFont(f);
  }
  textSize(size);

  float wordw = textWidth(word.text);
  fill(FG_COLOR);
  String cap = word.text.substring(0, 1).toUpperCase() + word.text.substring(1);
  text(cap, random(width - wordw), random(height - size));
  // saveFrame("frames/mw-######.png");
}

PFont f, bf;

int maxCount = 0;

// the list of word objects read from the csv files
ArrayList<Word> words = new ArrayList<Word>();

/*
 * class wrapping a word. note some attributes are not used yet
 */
class Word implements Comparable {

  /* the word string  */
  String text;

  /* the weight, understood as a number of occurence */
  int count;

  /* x position : not used yet */
  int x;

  /* y position : not used yet */
  int y;

  float size;

  int compareTo(Object other) {
    Word w = (Word) other;
    if(count < w.count) {
      return 1;
    } else if (count > w.count) {
      return -1;
    }
    return 0;
  }

}

void loadData() {
  
  Table table = loadTable(WORDS_CSV_FILE);

  println(table.getRowCount() + " total rows in table");

  for (TableRow row : table.rows()) {
    Word word = new Word();
    word.text = row.getString(0);
    word.count = row.getInt(1);
    if(word.text.length() > 0) {
      if(word.count > maxCount) {
        maxCount = word.count;
      }
      words.add(word);
    }
  }
  Collections.sort(words);
}