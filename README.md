# **Lexical-Attack**

Lexical Attack is a word display processor written with Processing, aimed at virtually shooting the spectator with a lexical field that comes from text statistics, like books or news articles.

Here is an example : https://www.youtube.com/watch?v=OPIhew2TjJY

It was made with the lexical field of Michel Foucault in "Discipline and Punish - The Birth of the Prison" (bundled in data/surveiller_et_punir.csv)

# Instructions

Input data is csv file with 2 columns : word text and score (count).

The tool [wordocc](https://git.tetalab.org:8888/Mutah/wordocc) can be used to produce such files from text input.

Open lexical_attack/lexical_attack.pde in Processing and read the top of the lexical_attack.pde file to modify the input data and the look.

And then ... launch the sketch !